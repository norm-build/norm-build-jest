const path = require("path");
const rootDir = module.parent.filename
  .split(path.sep)
  .slice(0, -1)
  .join(path.sep);
const mockFile = path.join(__dirname, "mock.js");
const flowTransformer = path.join(__dirname, "flow.js");
const typescriptTransformer = path.join(__dirname, "typescript.js");

const toExport = {
  rootDir,
  transform: {
    ".*.jsx?$": flowTransformer,
    ".*.tsx?$": typescriptTransformer
  },
  modulePaths: ["node_modules"],
  reporters: ["default", ["jest-junit", { suiteName: "React" }]],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json"],
  moduleNameMapper: {
    "\\.(css|less|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|md)$": mockFile
  },
  moduleDirectories: ["node_modules"],
  transformIgnorePatterns: ["node_modules/?!(react-native|native-base)/"]
};

const fs = require("fs");

function regexFindFilesSync(directory, unsanitisedRegex) {
  const regex = new RegExp(unsanitisedRegex);
  return fs.readdirSync(directory).filter(d => regex.test(d));
}

// Check to see if there is a global setup file to run
const setupFiles = regexFindFilesSync(rootDir, "jest.setup.[tj]sx?");
if (setupFiles.length) {
  if (setupFiles.length > 1) {
    throw new Error(
      `There can be at most one setup file; found ${
        setupFiles.length
      }: "${setupFiles.join('", "')}"`
    );
  }
  toExport.globalSetup = path.join(rootDir, setupFiles[0]);
}
// Check to see if there is a global teardown file to run
const teardownFiles = regexFindFilesSync(rootDir, "jest.teardown.[tj]sx?");
if (teardownFiles.length) {
  if (teardownFiles.length > 1) {
    throw new Error(
      `There can be at most one teardown file; found ${
        teardownFiles.length
      }: "${teardownFiles.join('", "')}"`
    );
  }
  toExport.globalTeardown = path.join(rootDir, teardownFiles[0]);
}

module.exports = toExport;
