# norm-build-jest

Adds a handful of jest packages and associated configuration - so you don't have to.

To use:

* install `norm-build-jest` and its peer dependency `norm-build`
  + `npm install norm-build-jest norm-build` if you use npm
  + `yarn add norm-build-jest norm-build` if you use yarn
* copy the "jest.config.js" file into the root of your repo.
  + it tells jest to use this module when it runs; its content is simply `module.exports = require('norm-build-jest')`

Usage:

* It works like jest but handles norm-build javascript files. Call a file `x.test.ts` and `yarn jest` will run it.
* Any test files ending in `.js` or `.jsx` can use flow
* Any test files ending in `.ts` or `.tsx` can use typescript

Bundled plugins:

* [jest-image-snapshot](https://github.com/americanexpress/jest-image-snapshot) with types
* [jest-junit](https://github.com/jest-community/jest-junit)

Bundled config:

* Auto detects global setup and teardown files in the root matching `jest\.setup\.[jt]sx?` and `jest\.teardown\.[jt]sx?`